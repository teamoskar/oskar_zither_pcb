EESchema Schematic File Version 4
LIBS:oskar_zither_pcb-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Oskar Zither PCB Schematic"
Date "2021-02-03"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_Push SW1
U 1 1 5F411D78
P 4100 3050
F 0 "SW1" H 4100 3335 50  0000 C CNN
F 1 "SW_Push" H 4100 3244 50  0000 C CNN
F 2 "keyswitches:PG1350-keycap" H 4100 3250 50  0001 C CNN
F 3 "" H 4100 3250 50  0001 C CNN
	1    4100 3050
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5F411DFE
P 4100 3450
F 0 "SW2" H 4100 3735 50  0000 C CNN
F 1 "SW_Push" H 4100 3644 50  0000 C CNN
F 2 "keyswitches:PG1350-keycap" H 4100 3650 50  0001 C CNN
F 3 "" H 4100 3650 50  0001 C CNN
	1    4100 3450
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5F4126AB
P 4100 3850
F 0 "SW3" H 4100 4135 50  0000 C CNN
F 1 "SW_Push" H 4100 4044 50  0000 C CNN
F 2 "keyswitches:PG1350-keycap" H 4100 4050 50  0001 C CNN
F 3 "" H 4100 4050 50  0001 C CNN
	1    4100 3850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW7
U 1 1 5F412713
P 4100 4250
F 0 "SW7" H 4100 4535 50  0000 C CNN
F 1 "SW_Push" H 4100 4444 50  0000 C CNN
F 2 "keyswitches:PG1350-keycap" H 4100 4450 50  0001 C CNN
F 3 "" H 4100 4450 50  0001 C CNN
	1    4100 4250
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 5F412B8E
P 3200 3050
F 0 "SW4" H 3200 3335 50  0000 C CNN
F 1 "SW_Push" H 3200 3244 50  0000 C CNN
F 2 "keyswitches:PG1350-keycap" H 3200 3250 50  0001 C CNN
F 3 "" H 3200 3250 50  0001 C CNN
	1    3200 3050
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 5F412B95
P 3200 3450
F 0 "SW5" H 3200 3735 50  0000 C CNN
F 1 "SW_Push" H 3200 3644 50  0000 C CNN
F 2 "keyswitches:PG1350-keycap" H 3200 3650 50  0001 C CNN
F 3 "" H 3200 3650 50  0001 C CNN
	1    3200 3450
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW6
U 1 1 5F412B9C
P 3200 3850
F 0 "SW6" H 3200 4135 50  0000 C CNN
F 1 "SW_Push" H 3200 4044 50  0000 C CNN
F 2 "keyswitches:PG1350-keycap" H 3200 4050 50  0001 C CNN
F 3 "" H 3200 4050 50  0001 C CNN
	1    3200 3850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW8
U 1 1 5F412BA3
P 3200 4250
F 0 "SW8" H 3200 4535 50  0000 C CNN
F 1 "SW_Push" H 3200 4444 50  0000 C CNN
F 2 "keyswitches:PG1350-keycap" H 3200 4450 50  0001 C CNN
F 3 "" H 3200 4450 50  0001 C CNN
	1    3200 4250
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW9
U 1 1 5F412BD5
P 3200 2450
F 0 "SW9" H 3200 2735 50  0000 C CNN
F 1 "SW_Push" H 3200 2644 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x02_P3.81mm_Drill0.8mm" H 3200 2650 50  0001 C CNN
F 3 "" H 3200 2650 50  0001 C CNN
	1    3200 2450
	1    0    0    -1  
$EndComp
$Comp
L pspice:R R1
U 1 1 5F413672
P 4650 3850
F 0 "R1" V 4855 3850 50  0000 C CNN
F 1 "1k" V 4764 3850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 4650 3850 50  0001 C CNN
F 3 "~" H 4650 3850 50  0001 C CNN
	1    4650 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 4450 3400 4250
Wire Wire Line
	3400 4250 3400 3850
Connection ~ 3400 4250
Wire Wire Line
	3400 3850 3400 3450
Connection ~ 3400 3850
Connection ~ 3400 3450
Wire Wire Line
	3400 4450 4300 4450
Wire Wire Line
	4300 4450 4300 4250
Wire Wire Line
	4300 4250 4300 3850
Connection ~ 4300 4250
Wire Wire Line
	4300 3850 4300 3450
Connection ~ 4300 3850
Connection ~ 4300 3450
Wire Wire Line
	4300 3050 4300 3450
Wire Wire Line
	3400 3050 3400 3450
Connection ~ 3400 3050
Text GLabel 5450 2750 0    50   Input ~ 0
dot1
Text GLabel 8050 2450 2    50   Input ~ 0
dot4
Text GLabel 3900 3050 0    50   Input ~ 0
dot1
Text GLabel 3000 3050 0    50   Input ~ 0
dot4
Text GLabel 5450 2850 0    50   Input ~ 0
dot2
Text GLabel 3900 3450 0    50   Input ~ 0
dot2
Text GLabel 3900 3850 0    50   Input ~ 0
dot3
Text GLabel 8050 2650 2    50   Input ~ 0
dot3
Text GLabel 3000 3450 0    50   Input ~ 0
dot5
Text GLabel 8050 2550 2    50   Input ~ 0
dot5
Text GLabel 3000 3850 0    50   Input ~ 0
dot6
Text GLabel 8050 2750 2    50   Input ~ 0
dot6
Text GLabel 3900 4250 0    50   Input ~ 0
dot7
Text GLabel 8050 2950 2    50   Input ~ 0
dot7
Text GLabel 3000 4250 0    50   Input ~ 0
dot8
Text GLabel 8050 2850 2    50   Input ~ 0
dot8
Text GLabel 3000 2450 0    50   Input ~ 0
thumb9
Text GLabel 8050 3750 2    50   Input ~ 0
thumb9
$Comp
L Mechanical:MountingHole H1
U 1 1 5F422718
P 4500 4750
F 0 "H1" H 4600 4796 50  0000 L CNN
F 1 "MountingHole" H 4600 4705 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 4500 4750 50  0001 C CNN
F 3 "~" H 4500 4750 50  0001 C CNN
	1    4500 4750
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5F4228B8
P 5400 4750
F 0 "H2" H 5500 4796 50  0000 L CNN
F 1 "MountingHole" H 5500 4705 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 5400 4750 50  0001 C CNN
F 3 "~" H 5400 4750 50  0001 C CNN
	1    5400 4750
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5F42293A
P 6350 4750
F 0 "H3" H 6450 4796 50  0000 L CNN
F 1 "MountingHole" H 6450 4705 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 6350 4750 50  0001 C CNN
F 3 "~" H 6350 4750 50  0001 C CNN
	1    6350 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3850 4300 3850
$Comp
L Switch:SW_Push SW10
U 1 1 5F7ADBF2
P 4100 2450
F 0 "SW10" H 4100 2735 50  0000 C CNN
F 1 "SW_Push" H 4100 2644 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x02_P3.81mm_Drill0.8mm" H 4100 2650 50  0001 C CNN
F 3 "" H 4100 2650 50  0001 C CNN
	1    4100 2450
	1    0    0    -1  
$EndComp
Text GLabel 8050 3650 2    50   Input ~ 0
thumb10
Text GLabel 3900 2450 0    50   Input ~ 0
thumb10
Wire Wire Line
	3400 2450 3400 3050
Wire Wire Line
	4300 2450 4300 3050
NoConn ~ 8050 3050
NoConn ~ 8050 3150
NoConn ~ 8050 3250
NoConn ~ 8050 3350
NoConn ~ 8050 3550
NoConn ~ 8050 3850
NoConn ~ 8050 4050
NoConn ~ 8050 4150
NoConn ~ 5450 4250
NoConn ~ 5450 4150
NoConn ~ 5450 4050
NoConn ~ 5450 3650
NoConn ~ 5450 3550
NoConn ~ 5450 3450
NoConn ~ 5450 3350
NoConn ~ 5450 3250
NoConn ~ 5450 3150
NoConn ~ 5450 3050
NoConn ~ 5450 2550
NoConn ~ 5450 2450
Wire Wire Line
	5450 3950 5300 3950
$Comp
L power:GND #PWR0102
U 1 1 6137B562
P 5300 3950
F 0 "#PWR0102" H 5300 3700 50  0001 C CNN
F 1 "GND" H 5305 3777 50  0000 C CNN
F 2 "" H 5300 3950 50  0001 C CNN
F 3 "" H 5300 3950 50  0001 C CNN
	1    5300 3950
	1    0    0    -1  
$EndComp
$Comp
L arduino:Arduino_Micro_Socket XA1
U 1 1 5F411C70
P 6750 3350
F 0 "XA1" H 6750 4587 60  0000 C CNN
F 1 "Arduino_Micro_Socket" H 6750 4481 60  0000 C CNN
F 2 "Arduino:Arduino_Micro_Socket_oskar" H 8550 7100 60  0001 C CNN
F 3 "https://store.arduino.cc/arduino-micro" H 8550 7100 60  0001 C CNN
	1    6750 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 6137BE92
P 5150 3850
F 0 "#PWR0103" H 5150 3600 50  0001 C CNN
F 1 "GND" H 5155 3677 50  0000 C CNN
F 2 "" H 5150 3850 50  0001 C CNN
F 3 "" H 5150 3850 50  0001 C CNN
	1    5150 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 3850 5150 3850
Wire Wire Line
	5150 3850 4900 3850
Connection ~ 5150 3850
$EndSCHEMATC
