# oskar_zither_pcb
PCB for Oskar Zither.
Oskar Zither is a open-source, mobile braille-keyboard.
The arrangement of 8 keys in a braille cell block and two additional keys allows Oskar Zither to be controlled without a supporting surface.

# Build
need [KiCad](http://www.kicad-pcb.org/)

or you can order the prepared PCB at Aisler.net. <https://aisler.net/p/OPSGPOBN>

# PCB Specifications
- Dimension 67.4mm x 70.5mm
- Layer count 2
- Base material FR4 TG 140°
- Nominal Thickness include Copper Cladding 1.6 mm

# Contact
[https://oskars.org](https://oskars.org)
Johannes Strelka-Petz <johannes_at_oskars.org>

# Copyright & License
    SPDX-FileCopyrightText: 2020 Johannes Strelka-Petz <johannes_at_oskars.org>
    SPDX-License-Identifier: CERN-OHL-S-2.0+
    ------------------------------------------------------------------------------
    | Copyright Johannes Strelka-Petz 2020.                                        |
    |                                                                              |
    | This source describes Open Hardware and is licensed under the CERN-OHL-S v2  |
    | or any later version.                                                        |
    |                                                                              |
    | You may redistribute and modify this source and make products using it under |
    | the terms of the CERN-OHL-S v2 or any later version                          |
    | (https://ohwr.org/cern_ohl_s_v2.txt).                                        |
    |                                                                              |
    | This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,          |
    | INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A         |
    | PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 or any later version        |
    | for applicable conditions.                                                   |
    |                                                                              |
    | Source location: https://gitlab.com/teamoskar/oskar_zither_pcb               |
    |                                                                              |
    | As per CERN-OHL-S v2 section 4, should You produce hardware based on this    |
    | source, You must where practicable maintain the Source Location visible      |
    | on the Oskar Zither PCB or other products you make using this source.        |
     ------------------------------------------------------------------------------
# Images
![schematic](images/oskar_zither_pcb.svg "schematic")
![front](images/oskar_zither_pcb-brd-front.svg "front")
![back](images/oskar_zither_pcb-brd-back.svg "back")
